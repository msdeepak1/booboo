$(document).ready(function(){
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 100,
      values: [ 0, 100 ],
      slide: function( event, ui ) {
        $( "#amount" ).val( "â‚¬" + ui.values[ 0 ] + " - â‚¬" + ui.values[ 1 ] );
      }
    });
    $( "#amount" ).val( "â‚¬" + $( "#slider-range" ).slider( "values", 0 ) +
      " - â‚¬" + $( "#slider-range" ).slider( "values", 1 ) );

});